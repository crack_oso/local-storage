// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

// create tab group
var tabGroup = Titanium.UI.createTabGroup();


///// LOCAL STORAGE //////

//Así se guarda local, esa pantalla se muestra en caso de que no sea algún idioma específico.
//Ti.App.Properties.setString('idioma', 'Español');
//Ti.API.info('The value of the givenName property is: ' + Ti.App.Properties.getString('idioma'));
//alert("el idioma es: " + Ti.App.Properties.getString('idioma'));


var datos = [
	{title: "Alemán", className: "tabla1", leftImage:"images/alemania.png" },
	{title: "Español", className: "tabla1", leftImage:"images/espana.png" },
	{title: "Francés", className: "tabla1", leftImage:"images/francia.png" },
	{title: "Inglés", className: "tabla1", leftImage:"images/inglaterra.png"},
	{title: "Italiano", className: "tabla1", leftImage:"images/italia.png" },
	{title: "Chino", className: "tabla1", leftImage:"images/portugal.png" },
];

var tableView = Titanium.UI.createTableView({
	data: datos
});

//alert("El idioma es: " + Ti.App.Properties.getString('idioma'));

var idioma='de_';

//pantalla de idioma 
var win1 = Titanium.UI.createWindow({  
    title: L(idioma+'seleccione'),
    backgroundColor:'#fff'
});

var tab1 = Titanium.UI.createTab({  
    icon:'KS_nav_views.png',
    title: L(idioma+'pagina'),
    window:win1
});

tableView.addEventListener('click', function(e){
	if(e.index==0)
	{
		Ti.App.Properties.setString('idioma', 'Alemán');
		idioma = 'de_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}
	if(e.index==1)
	{
		Ti.App.Properties.setString('idioma', 'Español');
		idioma = 'es_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}
	if(e.index==2)
	{
		Ti.App.Properties.setString('idioma', 'Francés');
		idioma = 'fr_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}
	if(e.index==3)
	{
		Ti.App.Properties.setString('idioma', 'Inglés');
 		idioma = 'en_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}
	if(e.index==4)
	{
		Ti.App.Properties.setString('idioma', 'Italiano');
		idioma = 'it_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}
	if(e.index==5)
	{
		Ti.App.Properties.setString('idioma', 'Chino');
		idioma = 'zh_';
        //alert("El idioma es: " + Ti.App.Properties.getString('idioma'));
        win1.title= L(idioma+'seleccione');
        tab1.title= L(idioma+'pagina');
	}

});

if (Titanium.Platform.name == 'iPhone' || Titanium.Platform.name == 'iPad') {
    // Android stuff

iads = Ti.UI.iOS.createAdView({
    width: 'auto',
    height: 'auto',
    top: -100,
    borderColor: '#000000',
    backgroundColor: '#000000'}); 
 
    t1 = Titanium.UI.createAnimation({top:0, duration:750}); 
 
    iads.addEventListener('load', function(){
        iads.animate(t1);
    }); 
} 

win1.add(tableView);
//win1.add(iads);

//
//  add tabs
//
tabGroup.addTab(tab1);  


// open tab group
tabGroup.open();
